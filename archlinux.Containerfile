FROM archlinux:latest

LABEL description="archlinux build container for gerbolyze"

ARG GERBOLYZE_DEPS="python-numpy python-slugify python-lxml python-click python-pillow librsvg"
ARG GERBONARA_DEPS="python-scipy python-sphinx python-pytest twine python-beautifulsoup4 kicad"

RUN pacman -Syyu --needed --noconfirm --noprogressbar base-devel git python python-pip python-setuptools python-wheel python-build make clang rustup pkgconf podman $GERBOLYZE_DEPS $GERBONARA_DEPS \
 && useradd --home-dir /home/bernd --create-home --shell /bin/sh --uid 1000 --user-group bernd

# Archlinux some time in early 2023 just randomly dropped the gerbv package out of its repos. I don't know what made
# them do that given that gerbv is a small, maintained, easy-to-build utility that literally everyone is using and that
# is present on pretty much every other distro, but whatever, we have to work around this.
# 
# Archlinux is great, but there is some really, really dumb stuff its maintainers do. One such really, really dumb thing
# is that makepkg refuses to run as root, and does not have an override for this check. Obviously, when running it from
# a dockerfile, that is not just unnecessary but actually counterproductive. Since this archlinux maintainer decided to
# force their opinion on makepkg users, we have to work around this bogus check in one of two ways:
# 
# 1. hot-patch makepkg
# 2. create a throwaway user account inside the container
# 
# We do (2), since I can't be bothered to do (1).

RUN pacman -S --needed --noconfirm sudo
RUN useradd builduser -m
RUN passwd -d builduser
RUN echo 'builduser ALL=(ALL) ALL' | tee -a /etc/sudoers
RUN sudo -u builduser bash -c 'cd ~ && git clone https://aur.archlinux.org/gerbv-git.git gerbv-git && cd gerbv-git && makepkg -s --noconfirm'

USER 1000

RUN rustup install stable \
 && rustup default stable \
 && rustup target add wasm32-wasi \
 && cargo install resvg usvg \
 && echo "export PATH=\$HOME/.local/bin:\$HOME/.cargo/bin:\$PATH" >> $HOME/.profile
